#!/bin/bash
# Display CPU temperatures and optionally hard disk temperature.

help="
usage: $0 [--oneline] [/dev/sda|cpu]...

Display disk and/or CPU temperatures.



The argument can either be a hard drive as a block device (e.g. /dev/sda),
to show the temperature of a hard drive, or the special value 'cpu', which
denotes that the temperature of all phyical CPU cores should be shown.

optional arguments:
  -o,  --oneline        display information on one line.

environment variables:
  PIGEON_TITLE          if defined, then print a title."

function show_help () {
  echo "$help"
  exit 0
}


# debug_msg [string...]
# Print optional debug messages to stderr
function debug_msg () {
  [ -v PIGEON_DEBUG ] || return
  >&2 tput dim
  >&2 echo "$@"
  >&2 tput sgr0
}

# get_nc -> string
# Produce the command used to run netcat.
# It's different depending on GNU or OpenPSD implementation
function get_nc () {
  if ! which nc > /dev/null; then
    debug_msg 'nc not found.'
    return 1
  fi

  if nc -h 2>&1 | grep -q 'GNU'; then
    debug_msg 'using gnu-netcat'
    echo 'nc'
  elif nc -h 2>&1 | grep -q 'OpenBSD'; then
    debug_msg 'using openbsd-netcat'
    echo 'nc -w 1 -d'
  else
    debug_msg 'cannot identify netcat version'
    return 1
  fi
}

# Type: A TempC is one of:
# - Nat
# - 'SLP'

# read_hddtemp_daemon /dev/sda -> TempC
# Try to get disk temperature by calling the hddtemp daemon.
function read_hddtemp_daemon () {
  local disk=$1

  if ! systemctl is-active -q hddtemp.service; then
    debug_msg 'hddtemp.service is not active'
    return 1
  fi

  local nc="$(get_nc)" || return 1

  local cmd="$nc localhost 7634"
  debug_msg "command: $cmd"

  # command is piped to strings to filter out weird symbols
  local res="$(sh -c "$cmd" | strings -s '')"
  if [ "$?" != '0' ]; then
    debug_msg "nc failed --- Maybe hddtemp daemon is not on the default port."
    return 1
  fi

  local res=$(grep -m 1 $disk <<< "$res")
  local temp=$(awk -F'|' '{print $4}' <<< "$res")
  local unit=$(awk -F'|' '{print $5}' <<< "$res")

  if [ "$temp" = 'SLP' ]; then
    echo $temp
    return
  fi

  if [ "$unit" != 'C' ]; then
    debug_msg "hddtemp response is not in Celcius."
    return 1
  fi

  echo $temp
}

# read_hddtemp_command /dev/sda -> TempC
# Try to get disk temperature by running the hddtemp command.
function read_hddtemp_command () {
  local disk=$1

  if ! which sudo hddtemp > /dev/null 2>&1; then
    debug_msg "sudo and/or hddtemp not found."
    return 1
  fi

  local temp=$(sudo -n -- hddtemp -n --unit=C $disk 2>&1)
  if [[ "$temp" =~ ^[0-9]+$ ]]; then
    echo $temp
  elif grep -q 'sleeping' <<< "$temp"; then
    echo 'SLP'
  else
    debug_msg "hddtemp returned '$temp', is not a number."
    return 1
  fi
}

# read_smartctl /dev/sda -> TempC
# try to get the disk temperature using smartctl.
function read_smartctl () {
  local disk=$1

  if ! which smartctl jq > /dev/null 2>&1; then
    debug_msg 'smartctl and/or jq not found.'
    return 1
  fi

  local temp=$(smartctl -Aj $disk | jq .temperature.current)
  if [[ "$temp" =~ '^[0-9]+$' ]]; then
    echo $temp
  else
    debug_msg "smartctl returned '$temp', is not a number."
    return 1
  fi
}

# hdd_temp /dev/sda -> tempC
# Try to read the disk temperature, somehow.
function hdd_temp () {
  read_hddtemp_daemon $1 || read_smartctl $1 || read_hddtemp_command $1
}


# tcolor tempC low mid high -> Color
# sets the color for a temperature.

function tcolor () {
  local temp="${1%.*}"  # cast float to int

  if ! [[ "$temp" =~ ^[0-9]+$ ]]; then
    tput setab 8  # grey, value is probably 'SLP'
  elif [ "$temp" -ge "$2" ]; then
    tput setab 160  # bright red
  elif [ "$temp" -ge "$3" ]; then
    tput setab 214  # yellow orange
  elif [ "$temp" -ge "$4" ]; then
    tput setab 34  # green
  else
    tput setab 6  # cyan
  fi
}


# parse arguments

disks=()
unset oneline
unset show_cpu

for arg in "$@"; do
  if [ "$arg" = "--help" ] || [ "$arg" = '-h' ]; then
    show_help
  elif [ "$arg" = "--oneline" ] || [ "$arg" = '-o' ]; then
    oneline=y
  elif [ -b "$arg" ]; then
    disks+=("$arg")
  elif [ "$arg" = 'cpu' ]; then
    show_cpu=y
  else
    echo "unrecognized argument: $arg"
    exit 1
  fi
done


if [ -n "$PIGEON_TITLE" ]; then
  tput sgr0
  tput bold
  printf "%s\n\n" "Hardware Temperatures"
  tput sgr0
fi


clear=$(tput sgr0)


for disk in "${disks[@]}"; do
  temp=$(hdd_temp $disk)
  color="$(tcolor $temp 50 40 25)"
  printf " $color %s %s°C $clear " "$disk" "$temp"
done

if [[ -v show_cpu ]]; then
  if ! [[ -v oneline ]]; then
    echo
  fi

  cpu_temps=""
  col=0
  sensors_output=$(sensors -uA 2> /dev/null)


  while read -r core_name; do
    temp=$(printf "$sensors_output" | grep -A1 "$core_name" | awk '{if(/_input:/) print $2}')
    temp=$(printf "%.1f" "$temp")
    color="$(tcolor $temp 70 60 25)"
    cpu_temps="${cpu_temps} $color CORE $(echo $core_name | tr -dc '0-9') $temp°C $clear "

    # print four cores per row
    if [[ ! -v oneline ]]; then
      ((col++))
      if [ "$col" -ge "4" ]; then
        printf "\n%s\n" "${cpu_temps}"
        cpu_temps=""
        col=0
      fi
    fi
  done <<< $(printf "$sensors_output" | grep Core)

  if [ -n "$cpu_temps" ]; then
    if [[ -v oneline ]]; then
      spacing="%s"
    else
      spacing="\n%s\n"
    fi
    printf "$spacing" "$cpu_temps"
  fi
fi
